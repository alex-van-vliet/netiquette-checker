const { ExtensionCommon } = ChromeUtils.import('resource://gre/modules/ExtensionCommon.jsm');
const { Services } = ChromeUtils.import('resource://gre/modules/Services.jsm');

// eslint-disable-next-line no-var, no-unused-vars
var composeMessage = class extends ExtensionCommon.ExtensionAPI {
  getAPI() {
    return {
      composeMessage: {
        async getSubject() {
          const win = Services.wm.getMostRecentWindow('msgcompose');
          const { editor } = win.gMsgSubjectElement;

          return editor.outputToString('text/plain', editor.eNone);
        },
        async getBody() {
          const win = Services.wm.getMostRecentWindow('msgcompose');
          const { editor } = win.gMsgCompose;

          return editor.outputToString('text/plain', editor.eNone);
        },
      },
    };
  }
};
