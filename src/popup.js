import './popup.scss';
import News from './news/news.js';
import NewsChecker from './news/news-checker.js';

import subjectRule from './news/rules/subject.js';
import bodyFormatRule from './news/rules/body/format.js';
import bodyMaxColsRule from './news/rules/body/max-cols.js';
import bodySignatureRule from './news/rules/body/signature.js';
import bodyQuoteRule from './news/rules/body/quote.js';

(async function main() {
  const checker = new NewsChecker();
  checker.add(subjectRule);
  checker.add(bodyFormatRule);
  checker.add(bodyMaxColsRule);
  checker.add(bodySignatureRule);
  checker.add(bodyQuoteRule);

  const news = new News(
    await browser.composeMessage.getSubject(),
    await browser.composeMessage.getBody(),
  );

  const [success, messages] = checker.run(news);

  const div = document.querySelector('.popup-page');
  if (success) {
    div.innerHTML = '<h1>Well done!</h1>';
  } else {
    div.innerHTML = `<h1>Wait!</h1><ul>${messages.map((e) => `<li>${e.message}</li>`).join('')}</ul>`;
  }
}());
