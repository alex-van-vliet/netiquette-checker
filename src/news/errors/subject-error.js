import NetiquetteError from './netiquette-error.js';

export default class SubjectError extends NetiquetteError {
  constructor(error, section) {
    super('subject', error, section);
  }
}
