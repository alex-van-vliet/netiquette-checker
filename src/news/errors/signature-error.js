import NetiquetteError from './netiquette-error.js';

export default class SignatureError extends NetiquetteError {
  constructor(error, section) {
    super('signature', error, section);
  }
}
