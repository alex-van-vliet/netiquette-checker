import NetiquetteError from './netiquette-error.js';

export default class QuoteError extends NetiquetteError {
  constructor(error, section) {
    super('quoting', error, section);
  }
}
