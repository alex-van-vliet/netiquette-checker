import NetiquetteError from './netiquette-error.js';

export default class BodyError extends NetiquetteError {
  constructor(error, section) {
    super('message body', error, section);
  }
}
