export default class NetiquetteError extends Error {
  constructor(type, error, section) {
    super(`Invalid ${type}: ${error} / see ${section}`);
    this.name = 'NetiquetteError';
    this.type = type;
    this.error = error;
    this.section = section;
  }
}
