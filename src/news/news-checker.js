export default class NewsChecker {
  constructor() {
    this.rules = [];
  }

  add(rule) {
    this.rules.push(rule);
  }

  run(news) {
    const results = [];

    const success = this.rules.reduce((acc, rule) => {
      const generator = rule(news);
      let currentSuccess = true;
      let message;
      for (message = generator.next(); !message.done; message = generator.next()) {
        currentSuccess = false;
        results.push(message.value);
      }
      if (message.value) {
        currentSuccess = false;
        results.push(message.value);
      }
      return acc && currentSuccess;
    }, true);

    return [success, results];
  }
}
