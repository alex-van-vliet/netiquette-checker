import News from './news.js';

test('a news has a subject', () => {
  const news = new News('subject', 'body');

  expect(news.subject())
    .toBe('subject');
});

test('a news has a body', () => {
  const news = new News('subject', 'body');

  expect(news.body())
    .toBe('body');
});
