export default class News {
  constructor(subject, body) {
    this.subject_ = subject;
    this.body_ = body;
  }

  subject() {
    return this.subject_;
  }

  body() {
    return this.body_;
  }
}
