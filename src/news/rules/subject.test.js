import News from '../news.js';
import NewsChecker from '../news-checker.js';
import subjectRule from './subject.js';
import SubjectError from '../errors/subject-error.js';

function getChecker() {
  const checker = new NewsChecker();

  checker.add(subjectRule);

  return checker;
}

test('a correctly written subject is valid', () => {
  const news = new News('[TEST][CHECKER] This is a test news', '');
  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([true, []]);
});

test('the subject is mandatory', () => {
  const news = new News('', '');
  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new SubjectError('Empty or undefined subject', '2.1.1'),
    ]]);
});

test('the subject should not exceed 80 characters', () => {
  const error = new SubjectError('Length exceeds 80 chars', '2.1.1.2');
  const news79Chars = new News('[TEST][CHECKER] This is a really long subject, this is less than 80 chars______');
  const news80Chars = new News('[TEST][CHECKER] This is a really long subject, this is exactly 80 characters____', '');
  const news81Chars = new News('[TEST][CHECKER] This is a really long subject, this is going more than 80 chars__');
  const checker = getChecker();

  expect(checker.run(news79Chars))
    .toStrictEqual([true, []]);

  expect(checker.run(news80Chars))
    .toStrictEqual([true, []]);

  expect(checker.run(news81Chars))
    .toStrictEqual([false, [error]]);
});

test('the subject should have tags and a summary', () => {
  const error = new SubjectError('Subject must have tags and a summary', '2.1.1');
  const oneTagError = new SubjectError('Subject cannot have only one tag', '2.1.1');
  const newsWithoutTags = new News('This is a subject', '');
  const newsWithOneTagWithoutSummary = new News('[TEST]', '');
  const newsWithOneTagWithSummary = new News('[TEST] This is a subject', '');
  const newsWithTwoTagWithoutSummary = new News('[TEST][CHECKER]', '');
  const newsWithThreeTagWithoutSummary = new News('[TEST][CHECKER][REALLY]', '');
  const newsWithThreeTagWithSummary = new News('[TEST][CHECKER][REALLY] This is a subject', '');
  const checker = getChecker();

  expect(checker.run(newsWithoutTags))
    .toStrictEqual([false, [error]]);

  expect(checker.run(newsWithOneTagWithoutSummary))
    .toStrictEqual([false, [error]]);

  expect(checker.run(newsWithOneTagWithSummary))
    .toStrictEqual([false, [error, oneTagError]]);

  expect(checker.run(newsWithTwoTagWithoutSummary))
    .toStrictEqual([false, [error]]);

  expect(checker.run(newsWithThreeTagWithoutSummary))
    .toStrictEqual([false, [error]]);

  expect(checker.run(newsWithThreeTagWithSummary))
    .toStrictEqual([false, [error]]);
});
