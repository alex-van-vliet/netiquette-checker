import SubjectError from '../errors/subject-error.js';

const VALID_SUBJECT = new RegExp(String.raw`^(?:Re: ?)?(?:\[[A-Z0-9-_+/]{1,10}\]){2} .*$`);
const ONE_TAG = new RegExp(String.raw`^(?:Re: ?)?(?:\[[A-Z0-9-_+/]{1,10}\]){1}\s*[^\[\s].*$`);

export default function* subjectRule(news) {
  const subject = news.subject();
  if (!subject) {
    return new SubjectError('Empty or undefined subject', '2.1.1');
  }
  if (subject.length > 80) {
    yield new SubjectError('Length exceeds 80 chars', '2.1.1.2');
  }
  if (subject.startsWith('Re: ')) {
    return null;
  }
  if (!VALID_SUBJECT.test(subject)) {
    yield new SubjectError('Subject must have tags and a summary', '2.1.1');
    if (ONE_TAG.test(subject)) {
      yield new SubjectError('Subject cannot have only one tag', '2.1.1');
    }
  }
  return null;
}
