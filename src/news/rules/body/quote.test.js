import News from '../../news.js';
import NewsChecker from '../../news-checker.js';
import QuoteError from '../../errors/quote-error.js';
import bodyQuoteRule from './quote.js';

function getChecker() {
  const checker = new NewsChecker();

  checker.add(bodyQuoteRule);

  return checker;
}

test('a correctly written body is valid', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should be successful.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([true, []]);
});

test('a quote should start with an empty line or an attribution line', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    '> This is a quote which follows an attribution.',
    '',
    '> This is a quote which follows an empty line.',
    '',
    'Test',
    'Second test',
    '> This quote is erroneous',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new QuoteError('Quote section must be preceded by an empty line or an attribution line (line 9)', '2.2.3.2'),
    ]]);
});

test('a quote should be attributed', () => {
  const news = new News('', [
    'Hello,',
    '',
    '> This is a quote which follows an empty line.',
    '',
    '> This is a quote which follows an empty line.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new QuoteError('Quote section must be attributed (line 2)', '2.2.3.3'),
    ]]);
});

test('quotes should be separated by an empty line', () => {
  const news = new News('', [
    'Hello,',
    '',
    'Attribution',
    '> This is a quote which follows an attribution line.',
    'Hi',
    '> This is a quote which follows another attribution line.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new QuoteError('Quoting sections must be separated by empty lines (line 4)', '2.2.3.2'),
    ]]);
});

test('quotes should have a space between the > and the message', () => {
  const news = new News('', [
    'Hello,',
    '',
    'Attribution',
    '>This is a quote which follows an attribution line.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new QuoteError('Quoting needs a space between the last \'>\' and its content (line 3)', '2.2.3.2'),
    ]]);
});

test('quotes should never have a space between two >', () => {
  const news = new News('', [
    'Hello,',
    '',
    'Attribution',
    '> > This is a quote which follows an attribution line.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new QuoteError('Quoting multiple times should use multiple \'>\' without spaces in between', '2.2.3.2'),
    ]]);
});
