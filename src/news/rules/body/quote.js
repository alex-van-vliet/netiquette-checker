import QuoteError from '../../errors/quote-error.js';

export default function* bodyQuoteRule(news) {
  const body = news.body();

  const splitBody = body.split('\n');

  let quoteAttributionFound = false;
  let isInQuote = false;
  let indexInBlock = -1;

  for (let i = 0; i < splitBody.length; i += 1) {
    const line = splitBody[i];
    if (line === '-- ') {
      break;
    }
    if (line === '') {
      indexInBlock = -1;
      isInQuote = false;
    } else {
      indexInBlock += 1;

      if (line.startsWith('>')) {
        if (!isInQuote && indexInBlock > 1) {
          return new QuoteError(`Quote section must be preceded by an empty line or an attribution line (line ${i})`, '2.2.3.2');
        }
        isInQuote = true;

        if (!quoteAttributionFound) {
          if (i > 0 && splitBody[i - 1] !== '') {
            quoteAttributionFound = true;
          } else {
            return new QuoteError(`Quote section must be attributed (line ${i})`, '2.2.3.3');
          }
        }

        for (let j = 0; j < line.length; j += 1) {
          if (line[j] === '>') {
            continue;
          } else if (line[j] === ' ') {
            if (j + 1 < line.length && line[j + 1] === '>') {
              return new QuoteError('Quoting multiple times should use multiple \'>\' without spaces in between', '2.2.3.2');
            }
            break;
          } else {
            return new QuoteError(`Quoting needs a space between the last '>' and its content (line ${i})`, '2.2.3.2');
          }
        }
      } else if (isInQuote) {
        return new QuoteError(`Quoting sections must be separated by empty lines (line ${i})`, '2.2.3.2');
      }
    }
  }

  return null;
}
