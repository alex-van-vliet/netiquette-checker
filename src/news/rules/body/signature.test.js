import News from '../../news.js';
import NewsChecker from '../../news-checker.js';
import SignatureError from '../../errors/signature-error.js';
import bodySignatureRule from './signature.js';

function getChecker() {
  const checker = new NewsChecker();

  checker.add(bodySignatureRule);

  return checker;
}

test('a correctly written body is valid', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should be successful.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([true, []]);
});

test('the signature is mandatory', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    'One more line.',
    '',
    'Regards,',
    '',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new SignatureError('Signature not found', '2.3'),
    ]]);
});

test('the signature should not be empty', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    'One more line.',
    '',
    'Regards,',
    '',
    '-- ',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new SignatureError('Signature must not be empty', '2.3'),
    ]]);
});

test('the signature should be more than 4 lines', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    'One more line.',
    '',
    'Regards,',
    '',
    '-- ',
    'Test',
    'Test',
    'Test',
    'Test',
    'Test',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new SignatureError('Signature too long', '2.3'),
    ]]);
});

test('the signature should not start with an empty line', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    'One more line.',
    '',
    'Regards,',
    '',
    '-- ',
    '',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new SignatureError('Signature must not start with an empty line', '2.3'),
    ]]);
});
