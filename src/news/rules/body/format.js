import BodyError from '../../errors/body-error.js';

const TRAILING_WHITESPACE = new RegExp(String.raw`^(?!>|(-- )).*\s$`);

export default function* bodyFormatRule(news) {
  const body = news.body();
  if (!body) {
    return new BodyError('Empty or undefined body', '2.2.1');
  }

  const splitBody = body.split('\n');
  if (splitBody.length < 8) {
    return new BodyError('A valid message has a minimum of 7 lines, '
      + 'as it needs at least a greeting/salutation line, a body and a signature', '2.2.1');
  }

  const signaturesCount = splitBody.filter((line) => line === '-- ').length;
  if (signaturesCount < 1) {
    return new BodyError('No signature found', '2.3');
  }
  if (signaturesCount > 1) {
    return new BodyError('Signature separation must be unique', '2.3');
  }

  for (let i = 0; i < splitBody.length; i += 1) {
    if (TRAILING_WHITESPACE.test(splitBody[i])) {
      return new BodyError(`Line ${i} has a trailing whitespace and is not a (valid) signature delimiter`, '2.2.2.5');
    }
  }

  if (splitBody[0] === '' || splitBody[1] !== '') {
    return new BodyError('No greeting line found. Please note that an empty line must be inserted after the greeting line', '2.2.1.1');
  }

  const signatureIndex = splitBody.indexOf('-- ');
  if (signatureIndex < 6 || splitBody[signatureIndex - 1] !== '' || splitBody[signatureIndex - 2] === '' || splitBody[signatureIndex - 3] !== '') {
    return new BodyError('No salutation line found. Please note that empty lines must be inserted before and after the salutation line', '2.2.1.1');
  }

  return null;
}
