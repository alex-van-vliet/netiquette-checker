import BodyError from '../../errors/body-error.js';

const OVER_80_ALLOWED = new RegExp(String.raw`^(?:>+ )?\[[0-9]{1,3}\] \w{2,5}://.*`);

export default function* bodyMaxColsRule(news) {
  const body = news.body();

  const splitBody = body.split('\n');

  for (let i = 0; i < splitBody.length; i += 1) {
    if (splitBody[i].length <= 72) {
      continue;
    } else if (OVER_80_ALLOWED.test(splitBody[i])) {
      continue;
    } else if (splitBody[i].length > 80) {
      yield new BodyError(`Line ${i} width exceeds 80 chars`, '2.2.2.1');
    } else if (!splitBody[i].startsWith('>')) {
      yield new BodyError(`Line ${i} width exceeds 72 chars without quoting`, '2.2.2.1');
    }
  }
}
