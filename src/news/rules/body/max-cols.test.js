import News from '../../news.js';
import NewsChecker from '../../news-checker.js';
import BodyError from '../../errors/body-error.js';
import bodyMaxColsRule from './max-cols.js';

function getChecker() {
  const checker = new NewsChecker();

  checker.add(bodyMaxColsRule);

  return checker;
}

test('a correctly written body is valid', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should be successful.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([true, []]);
});

test('a not quoted line should not exceed 72 chars', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful, there are too many characters here...',
    'This line is also really long, however, it is good enough, but just okay',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('Line 2 width exceeds 72 chars without quoting', '2.2.2.1'),
    ]]);
});

test('a quoted line should not exceed 80 chars', () => {
  const news = new News('', [
    'Hello,',
    '',
    '> This line is really long, but it is alright, it can go up to eighty characters',
    '> This news should not be successful, there are too many characters on this line!',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('Line 3 width exceeds 80 chars', '2.2.2.1'),
    ]]);
});
