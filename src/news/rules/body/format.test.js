import News from '../../news.js';
import NewsChecker from '../../news-checker.js';
import BodyError from '../../errors/body-error.js';
import bodyFormatRule from './format.js';

function getChecker() {
  const checker = new NewsChecker();

  checker.add(bodyFormatRule);

  return checker;
}

test('a correctly written body is valid', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should be successful.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([true, []]);
});

test('the body is mandatory', () => {
  const news = new News('', '');
  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('Empty or undefined body', '2.2.1'),
    ]]);
});

test('the body should not be less than 8 lines', () => {
  const news = new News('', [
    'Hello,',
    'This is too short.',
    'Regards,',
    '-- ',
  ].join('\n'));
  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('A valid message has a minimum of 7 lines, as it needs at least a greeting/salutation line, a body and a signature', '2.2.1'),
    ]]);
});

test('the signature separation is mandatory', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    '',
    'Regards,',
    '',
    '',
    'Signature',
  ].join('\n'));
  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('No signature found', '2.3'),
    ]]);
});

test('the signature separation is unique', () => {
  const news = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    '',
    'Regards,',
    '-- ',
    '-- ',
    'Signature',
  ].join('\n'));
  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('Signature separation must be unique', '2.3'),
    ]]);
});

test('there should be no trailing whitespace', () => {
  const news = new News('', [
    'Hello,',
    '',
    '> ',
    'This news should not be successful. ',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(news))
    .toStrictEqual([false, [
      new BodyError('Line 3 has a trailing whitespace and is not a (valid) signature delimiter', '2.2.2.5'),
    ]]);
});

test('the greeting line is mandatory', () => {
  const error = new BodyError('No greeting line found. Please note that an empty line must be inserted after the greeting line', '2.2.1.1');
  const newsWithoutBlankLine = new News('', [
    'Hello,',
    'Hi.',
    'This news should not be successful.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));
  const newsWithoutGreetingLine = new News('', [
    '',
    '',
    'This news should not be successful.',
    '',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(newsWithoutBlankLine))
    .toStrictEqual([false, [
      error,
    ]]);

  expect(checker.run(newsWithoutGreetingLine))
    .toStrictEqual([false, [
      error,
    ]]);
});

test('the salutation line is mandatory', () => {
  const error = new BodyError('No salutation line found. Please note that empty lines must be inserted before and after the salutation line', '2.2.1.1');
  const newsWithoutBlankLineAfter = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    '',
    'Regards,',
    'Hi',
    '-- ',
    'Signature',
  ].join('\n'));
  const newsWithoutBlankLineBefore = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    'Hi',
    'Regards,',
    '',
    '-- ',
    'Signature',
  ].join('\n'));
  const newsWithoutSalutationLine = new News('', [
    'Hello,',
    '',
    'This news should not be successful.',
    '',
    '',
    '',
    '-- ',
    'Signature',
  ].join('\n'));

  const checker = getChecker();

  expect(checker.run(newsWithoutBlankLineAfter))
    .toStrictEqual([false, [
      error,
    ]]);

  expect(checker.run(newsWithoutBlankLineBefore))
    .toStrictEqual([false, [
      error,
    ]]);

  expect(checker.run(newsWithoutSalutationLine))
    .toStrictEqual([false, [
      error,
    ]]);
});
