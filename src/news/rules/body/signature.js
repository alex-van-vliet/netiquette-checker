import SignatureError from '../../errors/signature-error.js';

export default function* bodySignatureRule(news) {
  const body = news.body();

  const splitBody = body.split('\n');

  const signatureIndex = splitBody.lastIndexOf('-- ');
  if (signatureIndex === -1) {
    return new SignatureError('Signature not found', '2.3');
  }
  if (signatureIndex + 1 === splitBody.length) {
    return new SignatureError('Signature must not be empty', '2.3');
  }

  if (splitBody.length - 1 - signatureIndex > 4) {
    return new SignatureError('Signature too long', '2.3');
  }

  if (splitBody[signatureIndex + 1] === '') {
    return new SignatureError('Signature must not start with an empty line', '2.3');
  }

  return null;
}
