import News from './news.js';
import NewsChecker from './news-checker.js';
import NetiquetteError from './errors/netiquette-error.js';

const successfulNews = new News('[TEST][CHECKER] This is a test news', [
  'Hello,',
  '',
  'This news should be successful.',
  '',
  'Regards,',
  '',
  '-- ',
  'Signature',
].join('\n'));

test('a news checker can be constructed', () => {
  const checker = new NewsChecker();

  expect(checker)
    .not
    .toBe(null);
});

test('rules can be added to a news checker', () => {
  const checker = new NewsChecker();

  // eslint-disable-next-line no-empty-function
  const successfulRule = jest.fn(function* () {
  });

  checker.add(successfulRule);

  expect(checker.run(successfulNews))
    .toStrictEqual([true, []]);
  expect(successfulRule.mock.calls.length)
    .toBe(1);

  const error = new NetiquetteError('type', 'error', 'section', false);
  const failedRule = jest.fn(function* () {
    yield error;
  });
  checker.add(failedRule);

  expect(checker.run(successfulNews))
    .toStrictEqual([false, [error]]);
  expect(successfulRule.mock.calls.length)
    .toBe(2);
  expect(failedRule.mock.calls.length)
    .toBe(1);
});

test('return does not continue processing errors for the current rule', () => {
  const checker = new NewsChecker();

  const normalError1 = new NetiquetteError('type', 'error1', 'section');
  const normalError2 = new NetiquetteError('type', 'error2', 'section');
  const firstRule = jest.fn(function* () {
    yield normalError1;
    return;
    // eslint-disable-next-line no-unreachable
    yield normalError1;
  });
  const secondRule = jest.fn(function* () {
    yield normalError2;
  });
  checker.add(firstRule);
  checker.add(secondRule);

  expect(checker.run(successfulNews))
    .toStrictEqual([false, [normalError1, normalError2]]);
  expect(firstRule.mock.calls.length)
    .toBe(1);
  expect(secondRule.mock.calls.length)
    .toBe(1);
});
