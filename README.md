# Netiquette Checker Addon

A netiquette checker right in Thunderbird!

## Usage

Download the `build-extension` artifact and unzip it.
Open thunderbird, click on the hamburger icon on the right of the search bar => addons => addons.
In the page that just opened, click on the gear icon => install addon from file, and select the extension.xpi.

Now, when writing a message, you should have a _NC_ icon on the top right of the window.
Click on it to run the checker in your written message!


## Authors

Written by Alex van Vliet - alex@vanvliet.pro  
Based on [Léodagan](https://gitlab.cri.epita.fr/cyril/leodagan), by Cyril `zarak` Duval - cyril@cri.epita.fr

## License

See LICENSE file.
