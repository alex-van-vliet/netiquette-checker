import { terser } from 'rollup-plugin-terser';
import copy from 'rollup-plugin-copy';
import scss from 'rollup-plugin-scss';
import { eslint } from 'rollup-plugin-eslint';

const apis = ['composeMessage'];

export default [
  ...apis.map((api) => ({
    input: `src/apis/${api}/implementation.js`,
    output: {
      dir: `dist/apis/${api}`,
      format: 'cjs',
      sourcemap: true,
    },
    context: 'browser',
    plugins: [
      eslint({
        include: [`src/apis/${api}/**/*.js`],
      }),
      copy({
        targets: [
          {
            src: [`src/apis/${api}/schema.json`, `src/apis/${api}/implementation.js`],
            dest: `dist/apis/${api}`,
          },
        ],
      }),
      !process.env.ROLLUP_WATCH && terser({
        compress: {
          top_retain: [api],
          warnings: true,
        },
        mangle: {
          reserved: [api],
        },
      }),
    ],
  })),
  {
    input: 'src/popup.js',
    output: {
      dir: 'dist',
      format: 'iife',
      sourcemap: true,
    },
    plugins: [
      eslint({
        exclude: ['node_modules/**', '**/*.scss', 'src/apis/**/*.js'],
      }),
      scss({
        output: 'dist/popup.css',
      }),
      copy({
        targets: [
          {
            src: ['src/manifest.json', 'src/popup.html', 'src/popup.js'],
            dest: 'dist',
          },
          {
            src: 'static/*',
            dest: 'dist',
          },
        ],
      }),
      !process.env.ROLLUP_WATCH && terser(),
    ],
  },
];
