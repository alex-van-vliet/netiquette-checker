module.exports = {
  extends: [
    'eslint-config-airbnb-base',
  ],
  env: {
    jest: true,
  },
  rules: {
    'import/extensions': ['error', 'always'],
    'no-underscore-dangle': 'off',
    'require-yield': 'off',
    'no-continue': 'off',
  },
  overrides: [
    {
      files: ['src/**/*.js', '!src/apis/**/*.js'],
      globals: {
        browser: 'readonly',
        document: 'readonly',
      }
    },
    {
      files: ['src/apis/**/*.js'],
      rules: {
        'class-methods-use-this': 'off',
      },
      globals: {
        ChromeUtils: 'readonly',
      }
    }
  ]
};
